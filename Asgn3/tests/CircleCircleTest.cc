#include "../include/Circle.h"
#include <gtest/gtest.h>

// Uncomment when you're ready

TEST(CircleCircle, Contained_Centered)
{
  //Inner Contained same center
  Circle inner = Circle(Point(0.0,0.0), 2.0);
  Circle outer = Circle(Point(0.0,0.0), 4.0);
  ASSERT_TRUE(inner.ContainedBy(outer));
}

TEST(CircleCircle, Contained_NotCentered){
  //Inner Contained different centers
  Circle inner2 = Circle(Point(2,2), 1);
  Circle outer2 = Circle(Point(0.0,0.0),5);
  ASSERT_TRUE(inner2.ContainedBy(outer2));
}

TEST(CircleCircle, Uncontained_NoOverlap){
  //Inner and Outer not overlapping
  Circle inner3 = Circle(Point(2,2), 1);
  Circle outer3 = Circle(Point(5,5),1);
  ASSERT_FALSE(inner3.ContainedBy(outer3));
}

TEST(CircleCircle, Uncontained_Surrounds){
  //Inner surrounds outer
  Circle outer4 = Circle(Point(0.0,0.0), 2.0);
  Circle inner4 = Circle(Point(0.0,0.0), 4.0);
  ASSERT_FALSE(inner4.ContainedBy(outer4));
}

TEST(CircleCircle, Contained_Perimeter){
   //Inner perimeter touches Outer perimeter, Inner is inside Outer (contained)
  Circle outer5 = Circle(Point(2.0,0.0), 4.0);
  Circle inner5 = Circle(Point(0.0,0.0), 2.0);
  ASSERT_TRUE(inner5.ContainedBy(outer5));
}

TEST(CircleCircle, Uncontained_Perimeter){
  //Inner perimeter touches Outer perimeter, Inner is outside Outer (not contained)
  Circle inner6 = Circle(Point(0,0), 5);
  Circle outer6 = Circle(Point(6,0), 1);
  ASSERT_FALSE(inner6.ContainedBy(outer6));
}

/* 
   You'll need to extend this by adding additional tests for:
    1. Inner and Outer intersect (not contained)
    2. Inner is entirely outside Outer (not contained)
    3. Inner surrounds Outer (not contained)
    4. Inner perimeter touches Outer perimeter, Inner is inside Outer (contained)
    5. Inner perimeter touches Outer perimeter, Inner is outside Outer (not contained)
*/
